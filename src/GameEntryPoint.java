import controller.SceneLoopController;
import view.Scene;

import javax.swing.*;

public class GameEntryPoint {

    public static final int WINDOW_WIDTH = 700;
    public static final int WINDOW_HEIGHT = 360;
    public static final String WINDOW_TITLE = "Super Mario";

    public static void main(String[] args) {
        JFrame window = new JFrame(WINDOW_TITLE);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.setLocationRelativeTo(null);
        window.setResizable(true);
        window.setAlwaysOnTop(true);

        Scene scene = new Scene();

        window.setContentPane(scene);
        window.setVisible(true);

        Thread timer = new Thread(new SceneLoopController());
        timer.start();
    }

}

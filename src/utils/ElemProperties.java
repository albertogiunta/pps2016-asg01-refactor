package utils;

import java.awt.*;

public enum ElemProperties {

    TUNNEL (43, 65, CONST_RESOURCES.IMG_TUNNEL),
    BLOCK(30, 30, CONST_RESOURCES.IMG_BLOCK),
    COIN(30, 30, CONST_RESOURCES.IMG_PIECE1),
    MARIO(28,50, CONST_RESOURCES.IMG_MARIO_DEFAULT),
    TURTLE(43, 50, CONST_RESOURCES.IMG_TURTLE_IDLE),
    MUSHROOM(27, 30, CONST_RESOURCES.IMG_MUSHROOM_DEFAULT);

    private final int width;
    private final int height;
    private final String imageName;

    ElemProperties(int width, int height, String imageName) {
        this.width = width;
        this.height = height;
        this.imageName = imageName;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getImageName() {
        return imageName;
    }

    public Image getImageRes() {
        return ResourcesUtils.getImage(imageName);
    }
}

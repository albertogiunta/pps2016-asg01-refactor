package utils;

import model.gameElement.GameElement;
import model.gameElement.character.Character;

public class CollisionHelper {

    public static final int PROXIMITY_MARGIN = 10;
    public static final int BOH_MARGIN = 5;

    public static boolean hitAhead(GameElement hitterObj, GameElement hitObj) {
        if (hitterObj.getX() + hitterObj.getWidth() < hitObj.getX() || hitterObj.getX() + hitterObj.getWidth() > hitObj.getX() + 5 ||
                hitterObj.getY() + hitterObj.getHeight() <= hitObj.getY() || hitterObj.getY() >= hitObj.getY() + hitObj.getHeight()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean hitAhead(Character hitterObj, GameElement hitObj) {
        if (hitterObj.isToRight()) {
            return hitAhead(((GameElement) hitterObj), hitObj);
        } else {
            return false;
        }
    }

    public static boolean hitBack(GameElement hitterObj, GameElement hitObj) {
        if (hitterObj.getX() > hitObj.getX() + hitObj.getWidth() || hitterObj.getX() + hitterObj.getWidth() < hitObj.getX() + hitObj.getWidth() - 5 ||
                hitterObj.getY() + hitterObj.getHeight() <= hitObj.getY() || hitterObj.getY() >= hitObj.getY() + hitObj.getHeight()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean hitBelow(GameElement hitterObj, GameElement hitObj) {
        int offset = hitObj instanceof Character ? BOH_MARGIN : 0;
        if (hitterObj.getX() + hitterObj.getWidth() < hitObj.getX() + offset || hitterObj.getX() > hitObj.getX() + hitObj.getWidth() - 5 ||
                hitterObj.getY() + hitterObj.getHeight() < hitObj.getY() || hitterObj.getY() + hitterObj.getHeight() > hitObj.getY() + 5) {
            return false;
        } else
            return true;
    }

    public static boolean hitAbove(GameElement hitterObj, GameElement hitObj) {
        if (hitterObj.getX() + hitterObj.getWidth() < hitObj.getX() + 5 || hitterObj.getX() > hitObj.getX() + hitObj.getWidth() - 5 ||
                hitterObj.getY() < hitObj.getY() + hitObj.getHeight() || hitterObj.getY() > hitObj.getY() + hitObj.getHeight() + 5) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isNearby(GameElement hitterObj, GameElement hitObj) {
        if ((hitterObj.getX() > hitObj.getX() - PROXIMITY_MARGIN && hitterObj.getX() < hitObj.getX() + hitObj.getWidth() + PROXIMITY_MARGIN) ||
                (hitterObj.getX() + hitterObj.getWidth() > hitObj.getX() - PROXIMITY_MARGIN && hitterObj.getX() + hitterObj.getWidth() < hitObj.getX() + hitObj.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }
}

package model.platform;

import model.platform.level.LevelStrategy;
import model.gameElement.character.Mario;
import model.gameElement.character.enemy.Mushroom;
import model.gameElement.character.enemy.Turtle;
import model.gameElement.object.Coin;
import model.gameElement.object.StillObject;
import utils.CONST_RESOURCES;
import utils.ResourcesUtils;

import java.awt.*;
import java.util.List;

public class Platform {

    private List<StillObject> objects;
    private List<Coin> coins;

    private Mario mario;
    private Turtle turtle;
    private Mushroom mushroom;

    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;
    private Image imgFlag;
    private Image imgCastle;

    public Platform(LevelStrategy strategy) {

        this.imgBackground1 = ResourcesUtils.getImage(CONST_RESOURCES.IMG_BACKGROUND);
        this.imgBackground2 = ResourcesUtils.getImage(CONST_RESOURCES.IMG_BACKGROUND);
        this.castle = ResourcesUtils.getImage(CONST_RESOURCES.IMG_CASTLE);
        this.start = ResourcesUtils.getImage(CONST_RESOURCES.START_ICON);
        this.imgCastle = ResourcesUtils.getImage(CONST_RESOURCES.IMG_CASTLE_FINAL);
        this.imgFlag = ResourcesUtils.getImage(CONST_RESOURCES.IMG_FLAG);

        this.mario = strategy.createMario();
        this.turtle = strategy.createTurtle();
        this.mushroom = strategy.createMushroom();
        this.objects = strategy.disposeStillObjects();
        this.coins = strategy.disposeCoins();

    }

    public List<StillObject> getObjects() {
        return objects;
    }

    public List<Coin> getCoins() {
        return coins;
    }

    public Mario getMario() {
        return mario;
    }

    public Turtle getTurtle() {
        return turtle;
    }

    public Mushroom getMushroom() {
        return mushroom;
    }

    public Image getImgBackground1() {
        return imgBackground1;
    }

    public Image getImgBackground2() {
        return imgBackground2;
    }

    public Image getCastle() {
        return castle;
    }

    public Image getStart() {
        return start;
    }

    public Image getImgFlag() {
        return imgFlag;
    }

    public Image getImgCastle() {
        return imgCastle;
    }
}

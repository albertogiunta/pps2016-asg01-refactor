package model.platform.level;

import model.gameElement.character.Mario;
import model.gameElement.character.enemy.Mushroom;
import model.gameElement.character.enemy.Turtle;
import model.gameElement.object.Coin;
import model.gameElement.object.StillObject;

import java.util.List;

public interface LevelStrategy {

    List<StillObject> disposeStillObjects();

    List<Coin> disposeCoins();

    Mario createMario();

    Turtle createTurtle();

    Mushroom createMushroom();

}

package model.platform.level;

import model.gameElement.ConcreteObjectFactory;
import model.gameElement.ObjectFactory;
import model.gameElement.character.Mario;
import model.gameElement.character.enemy.Mushroom;
import model.gameElement.character.enemy.Turtle;
import model.gameElement.object.Coin;
import model.gameElement.object.StillObject;

import java.util.LinkedList;
import java.util.List;

public class FirstLevel implements LevelStrategy {

    private ObjectFactory factory;
    private List<StillObject> objects;
    private List<Coin> coins;

    public FirstLevel() {
        factory = ConcreteObjectFactory.getInstance();
        objects = new LinkedList<>();
        coins = new LinkedList<>();
        disposeAllObjects();
    }

    private void disposeAllObjects() {
        objects.add(factory.getNewBlock(400, 180));
        objects.add(factory.getNewBlock(400, 180));
        objects.addAll(factory.getNewInlineBlockConfiguration(400, 180, 100, 0, 2));
        coins.addAll(factory.getNewInlineCoinConfiguration(400, 125, 100, -30, 3));
        coins.addAll(factory.getNewInlineCoinConfiguration(700, 155, 50, 0, 4));
        objects.add(factory.getNewTunnel(600, 230));
        objects.add(factory.getNewTunnel(1000, 230));
        objects.add(factory.getNewBlock(1200, 180));
        coins.add(factory.getNewCoin(1202, 140));
        objects.add(factory.getNewBlock(1270, 170));
        coins.add(factory.getNewCoin(1272, 95));
        objects.add(factory.getNewBlock(1340, 160));
        coins.add(factory.getNewCoin(1342, 40));
        objects.add(factory.getNewTunnel(1600, 230));
        coins.add(factory.getNewCoin(1650, 145));
        objects.add(factory.getNewTunnel(1900, 230));
        objects.add(factory.getNewBlock(2000, 180));
        objects.add(factory.getNewTunnel(2500, 230));
        objects.add(factory.getNewBlock(2600, 160));
        objects.add(factory.getNewBlock(2650, 180));
        coins.add(factory.getNewCoin(2650, 145));
        coins.add(factory.getNewCoin(3000, 135));
        objects.add(factory.getNewTunnel(3000, 230));
        coins.add(factory.getNewCoin(3400, 125));
        objects.add(factory.getNewBlock(3500, 160));
        objects.add(factory.getNewBlock(3550, 140));
        objects.add(factory.getNewTunnel(3800, 230));
        coins.add(factory.getNewCoin(4200, 145));
        objects.add(factory.getNewTunnel(4500, 230));
        coins.add(factory.getNewCoin(4600, 40));
    }

    @Override
    public List<StillObject> disposeStillObjects() {
        return objects;
    }

    @Override
    public List<Coin> disposeCoins() {
        return coins;
    }

    @Override
    public Mario createMario() {
        return factory.getNewMario(300, 245);
    }

    @Override
    public Turtle createTurtle() {
        return factory.getNewTurtle(950, 243);
    }

    @Override
    public Mushroom createMushroom() {
        return factory.getNewMushroom(800, 263);
    }
}

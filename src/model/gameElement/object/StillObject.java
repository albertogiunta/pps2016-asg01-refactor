package model.gameElement.object;

import model.gameElement.BaseGameElement;

import java.awt.*;

public abstract class StillObject extends BaseGameElement {

    public StillObject(int x, int y, int width, int height, Image imgObj) {
        super(x, y, width, height, imgObj);
    }

}

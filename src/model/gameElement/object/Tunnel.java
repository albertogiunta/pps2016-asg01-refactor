package model.gameElement.object;

import static utils.ElemProperties.TUNNEL;

public class Tunnel extends StillObject {

    public Tunnel(int x, int y) {
        super(x, y, TUNNEL.getWidth(), TUNNEL.getHeight(), TUNNEL.getImageRes());
    }

}

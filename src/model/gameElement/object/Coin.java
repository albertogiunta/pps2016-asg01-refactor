package model.gameElement.object;

import model.gameElement.AnimatedGameObject;
import utils.CONST_RESOURCES;
import utils.ResourcesUtils;

import java.awt.*;

import static utils.ElemProperties.COIN;

public class Coin extends AnimatedGameObject {

    public static final int FLIP_FREQUENCY = 10;
    private int counter;

    public Coin(int x, int y) {
        super(x, y, COIN.getWidth(), COIN.getHeight(), COIN.getImageRes());
    }

    public Image imageOnMovement() {
        return ResourcesUtils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? CONST_RESOURCES.IMG_PIECE1 : CONST_RESOURCES.IMG_PIECE2);
    }
}

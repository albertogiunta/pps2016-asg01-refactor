package model.gameElement.object;

import static utils.ElemProperties.BLOCK;

public class Block extends StillObject {

    public Block(int x, int y) {
        super(x, y, BLOCK.getWidth(), BLOCK.getWidth(), BLOCK.getImageRes());
    }

}

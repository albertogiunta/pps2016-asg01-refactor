package model.gameElement;

import model.gameElement.character.Mario;
import model.gameElement.character.enemy.Mushroom;
import model.gameElement.character.enemy.Turtle;
import model.gameElement.object.Block;
import model.gameElement.object.Coin;
import model.gameElement.object.Tunnel;

import java.util.List;

public interface ObjectFactory {

    Coin getNewCoin(int x, int y);

    List<Coin> getNewInlineCoinConfiguration(int x, int y, int xStep, int yStep, int numberOfElements);

    Block getNewBlock(int x, int y);

    List<Block> getNewInlineBlockConfiguration(int x, int y, int xStep, int yStep, int numberOfElements);

    Tunnel getNewTunnel(int x, int y);

    Mario getNewMario(int x, int y);

    Turtle getNewTurtle(int x, int y);

    Mushroom getNewMushroom(int x, int y);
}

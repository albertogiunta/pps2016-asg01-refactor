package model.gameElement;

import java.awt.*;

public interface GameElement {

    int getX();

    void setX(int x);

    int getY();

    void setY(int y);

    int getWidth();

    void setWidth(int width);

    int getHeight();

    void setHeight(int height);

    Image getImgObj();

    void move();
}

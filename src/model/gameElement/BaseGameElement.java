package model.gameElement;

import controller.PlatformController;

import java.awt.*;

public class BaseGameElement implements GameElement {

    private int x;
    private int y;
    private int width;
    private int height;
    private Image imgObj;

    public BaseGameElement(int x, int y, int width, int height, Image imgObj) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.imgObj = imgObj;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public Image getImgObj() {
        return imgObj;
    }

    @Override
    public void move() {
        if (PlatformController.getInstance().getScene().getxPos() >= 0) {
            this.x = this.x - PlatformController.getInstance().getScene().getMov();
        }
    }
}

package model.gameElement;

import java.awt.*;

public abstract class AnimatedGameObject extends BaseGameElement implements Runnable {

    private static final int PAUSE = 10;

    public AnimatedGameObject(int x, int y, int width, int height, Image imgObj) {
        super(x, y, width, height, imgObj);
    }

    public abstract Image imageOnMovement();

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }
}

package model.gameElement.character.enemy;

import utils.CONST_RESOURCES;
import utils.ResourcesUtils;

import java.awt.*;

import static utils.ElemProperties.MUSHROOM;

public class Mushroom extends BaseEnemy {

    private Image img;
    private final int PAUSE = 15;

    public Mushroom(int x, int y) {
        super(x, y, MUSHROOM.getWidth(), MUSHROOM.getHeight(), MUSHROOM.getImageRes(), 1);
    }

    @Override
    public Image getDeadImage() {
        return ResourcesUtils.getImage(this.isToRight() ? CONST_RESOURCES.IMG_MUSHROOM_DEAD_DX : CONST_RESOURCES.IMG_MUSHROOM_DEAD_SX);
    }
}

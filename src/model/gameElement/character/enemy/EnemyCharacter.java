package model.gameElement.character.enemy;

import model.gameElement.BaseGameElement;
import model.gameElement.character.Character;

import java.awt.*;

public interface EnemyCharacter extends Character, Runnable {

    void setOffsetX(int offsetX);

    int getOffsetX();

    void contact(BaseGameElement obj);

    Image getDeadImage();

}

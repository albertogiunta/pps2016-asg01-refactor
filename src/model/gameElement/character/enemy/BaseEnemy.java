package model.gameElement.character.enemy;

import model.gameElement.BaseGameElement;
import model.gameElement.character.BasicCharacter;
import utils.CollisionHelper;

import java.awt.*;

public abstract class BaseEnemy extends BasicCharacter implements EnemyCharacter {

    private final int PAUSE = 15;

    private int offsetX;

    public BaseEnemy(int x, int y, int width, int height, Image objImg, int offsetX) {
        super(x, y, width, height, objImg);
        this.offsetX = offsetX;

        Thread chronoEnemy = new Thread(this);
        chronoEnemy.start();
    }

    @Override
    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }

    @Override
    public int getOffsetX() {
        return this.offsetX;
    }

    @Override
    public void contact(BaseGameElement obj) {
        if (CollisionHelper.hitAhead(this, obj) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (CollisionHelper.hitBack(this, obj) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    @Override
    public void run() {
        while (true) {
            if (super.isAlive()) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    @Override
    public void move() {
        offsetX = isToRight() ? 1 : -1;
        super.setX(super.getX() + offsetX);
    }

    @Override
    public abstract Image getDeadImage();
}

package model.gameElement.character.enemy;

import utils.CONST_RESOURCES;
import utils.ResourcesUtils;

import java.awt.*;

import static utils.ElemProperties.TURTLE;

public class Turtle extends BaseEnemy {

    public Turtle(int X, int Y) {
        super(X, Y, TURTLE.getWidth(), TURTLE.getHeight(), TURTLE.getImageRes(), 1);
    }

    @Override
    public Image getDeadImage() {
        return ResourcesUtils.getImage(CONST_RESOURCES.IMG_TURTLE_DEAD);
    }
}

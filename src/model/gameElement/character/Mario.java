package model.gameElement.character;

import view.Scene;
import controller.PlatformController;
import model.gameElement.BaseGameElement;
import model.gameElement.object.Coin;
import utils.CollisionHelper;
import utils.CONST_RESOURCES;
import utils.ResourcesUtils;

import java.awt.*;

import static utils.CollisionHelper.hitAbove;
import static utils.ElemProperties.MARIO;

public class Mario extends BasicCharacter {

    public static final int MARIO_OFFSET_Y_INITIAL = 243;
    public static final int FLOOR_OFFSET_Y_INITIAL = 293;
    public static final int JUMPING_LIMIT = 42;

    private Scene scene;

    private boolean jumping;
    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, MARIO.getWidth(), MARIO.getHeight(), MARIO.getImageRes());
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        scene = PlatformController.getInstance().getScene();
        String str;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > scene.getHeightLimit())
                this.setY(this.getY() - 4);
            else this.jumpingExtent = JUMPING_LIMIT;

            str = this.isToRight() ? CONST_RESOURCES.IMG_MARIO_SUPER_DX : CONST_RESOURCES.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < scene.getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            str = this.isToRight() ? CONST_RESOURCES.IMG_MARIO_SUPER_DX : CONST_RESOURCES.IMG_MARIO_SUPER_SX;
        } else {
            str = this.isToRight() ? CONST_RESOURCES.IMG_MARIO_ACTIVE_DX : CONST_RESOURCES.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return ResourcesUtils.getImage(str);
    }

    public void contact(BaseGameElement obj) {
        scene = PlatformController.getInstance().getScene();
        if (CollisionHelper.hitAhead(this, obj) && this.isToRight() || CollisionHelper.hitBack(this, obj) && !this.isToRight()) {
            scene.setMov(0);
            this.setMoving(false);
        }

        if (CollisionHelper.hitBelow(this, obj) && this.jumping) {
            scene.setFloorOffsetY(obj.getY());
        } else if (!CollisionHelper.hitBelow(this, obj)) {
            scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(this, obj)) {
                scene.setHeightLimit(obj.getY() + obj.getHeight()); // the new sky goes below the object
            } else if (!hitAbove(this, obj) && !this.jumping) {
                scene.setHeightLimit(0); // initial sky
            }
        }
    }

    public boolean contactPiece(Coin coin) {
        if (CollisionHelper.hitBack(this, coin) || CollisionHelper.hitAbove(this, coin) || CollisionHelper.hitAhead(this, coin)
                || CollisionHelper.hitBelow(this, coin))
            return true;

        return false;
    }

    public void contact(BasicCharacter pers) {
        if (CollisionHelper.hitAhead(this, pers) || CollisionHelper.hitBack(this, pers)) {
            if (pers.isAlive()) {
                this.setMoving(false);
                this.setAlive(false);
            } else setAlive(true);
        } else if (CollisionHelper.hitBelow(this, pers)) {
            pers.setMoving(false);
            pers.setAlive(false);
        }
    }
}

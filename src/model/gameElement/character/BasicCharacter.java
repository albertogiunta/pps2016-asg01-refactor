package model.gameElement.character;

import model.gameElement.BaseGameElement;
import utils.CONST_RESOURCES;
import utils.ResourcesUtils;

import java.awt.*;

public class BasicCharacter extends BaseGameElement implements Character {

    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;

    public BasicCharacter(int x, int y, int width, int height, Image objImg) {
        super(x, y, width, height, objImg);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    @Override
    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public int getCounter() {
        return counter;
    }

    @Override
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    @Override
    public boolean isMoving() {
        return moving;
    }

    @Override
    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    @Override
    public boolean isToRight() {
        return toRight;
    }

    @Override
    public Image walk(String name, int frequency) {
        String str = CONST_RESOURCES.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? CONST_RESOURCES.IMGP_STATUS_ACTIVE : CONST_RESOURCES.IMGP_STATUS_NORMAL) +
                (this.toRight ? CONST_RESOURCES.IMGP_DIRECTION_DX : CONST_RESOURCES.IMGP_DIRECTION_SX) + CONST_RESOURCES.IMG_EXT;
        return ResourcesUtils.getImage(str);
    }
}

package model.gameElement.character;

import model.gameElement.GameElement;

import java.awt.Image;

public interface Character extends GameElement {

	void setCounter(int counter);

	int getCounter();

	void setAlive(boolean alive);

	boolean isAlive();

	void setMoving(boolean moving);

	boolean isMoving();

	void setToRight(boolean toRight);

	boolean isToRight();

	Image walk(String name, int frequency);

}

package model.gameElement;

import model.gameElement.character.Mario;
import model.gameElement.character.enemy.Mushroom;
import model.gameElement.character.enemy.Turtle;
import model.gameElement.object.Block;
import model.gameElement.object.Coin;
import model.gameElement.object.Tunnel;

import java.util.LinkedList;
import java.util.List;

public class ConcreteObjectFactory implements ObjectFactory {

    private static ObjectFactory instance;

    public static ObjectFactory getInstance() {
        if (instance == null) {
            instance = new ConcreteObjectFactory();
        }
        return instance;
    }

    private ConcreteObjectFactory() {}

    @Override
    public Coin getNewCoin(int x, int y) {
        return new Coin(x, y);
    }

    @Override
    public Block getNewBlock(int x, int y) {
        return new Block(x, y);
    }

    @Override
    public List<Coin> getNewInlineCoinConfiguration(int x, int y, int xStep, int yStep, int numberOfElements) {
        int currentXStep = 0;
        int currentYStep = 0;
        List<Coin> list = new LinkedList<>();
        for (int i = 0; i < numberOfElements; i++) {
            list.add(this.getNewCoin(x + currentXStep, y + currentYStep));
            currentXStep += xStep;
            currentYStep += yStep;
        }
        return list;
    }

    @Override
    public List<Block> getNewInlineBlockConfiguration(int x, int y, int xStep, int yStep, int numberOfElements) {
        int currentXStep = 0;
        int currentYStep = 0;
        List<Block> list = new LinkedList<>();
        for (int i = 0; i < numberOfElements; i++) {
            list.add(this.getNewBlock(x + currentXStep, y + currentYStep));
            currentXStep += xStep;
            currentYStep += yStep;
        }
        return list;
    }

    @Override
    public Tunnel getNewTunnel(int x, int y) {
        return new Tunnel(x, y);
    }

    @Override
    public Mario getNewMario(int x, int y) {
        return new Mario(x, y);
    }

    @Override
    public Turtle getNewTurtle(int x, int y) {
        return new Turtle(x, y);
    }

    @Override
    public Mushroom getNewMushroom(int x, int y) {
        return new Mushroom(x, y);
    }

}

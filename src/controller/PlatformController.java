package controller;

import model.platform.level.FirstLevel;
import model.gameElement.GameElement;
import model.platform.Platform;
import model.gameElement.object.Coin;
import model.gameElement.object.StillObject;
import utils.CollisionHelper;
import utils.CONST_RESOURCES;
import view.Scene;

import java.util.LinkedList;
import java.util.List;

public class PlatformController {

    private static PlatformController instance;
    private Platform platform;
    private Scene scene;

    public static PlatformController getInstance() {
        if (instance == null) {
            instance = new PlatformController();
        }
        return instance;
    }

    private PlatformController() {
        platform = new Platform(new FirstLevel());
    }

    public void checkContacts() {

        for (StillObject stillObject : getObjects()) {
            if (CollisionHelper.isNearby(platform.getMario(), stillObject)) {
                platform.getMario().contact(stillObject);
            }

            if (CollisionHelper.isNearby(platform.getMushroom(), stillObject)) {
                platform.getMushroom().contact(stillObject);
            }

            if (CollisionHelper.isNearby(platform.getTurtle(), stillObject)) {
                platform.getTurtle().contact(stillObject);
            }
        }

        for (int i = 0; i < platform.getCoins().size(); i++) {
            Coin coin = platform.getCoins().get(i);
            if (platform.getMario().contactPiece(coin)) {
                AudioController.playSound(CONST_RESOURCES.AUDIO_MONEY);
                platform.getCoins().remove(i);
            }
        }
    }

    public void checkProximity() {
        if (CollisionHelper.isNearby(platform.getMario(), platform.getMushroom())) {
            platform.getMario().contact(platform.getMushroom());
        }

        if (CollisionHelper.isNearby(platform.getMario(), platform.getTurtle())) {
            platform.getMario().contact(platform.getTurtle());
        }
    }

    public void moveAll() {
        for (StillObject object : getObjects()) {
            object.move();
        }

        for (Coin coin : platform.getCoins()) {
            coin.move();
        }

        platform.getMushroom().move();
        platform.getTurtle().move();
    }

    public List<StillObject> getObjects() {
        return new LinkedList<>(platform.getObjects());
    }

    public List<GameElement> getAllGameElements() {
        List<GameElement> list = new LinkedList<>(platform.getObjects());
        list.addAll(platform.getCoins());
        return list;
    }

    public Platform getPlatform() {
        return platform;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}

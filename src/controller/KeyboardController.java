package controller;

import view.Scene;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardController implements KeyListener {

    public static final int GO_LEFT = 1;
    public static final int GO_RIGHT = -1;
    public static final int STOP = 0;

    Scene scene;

    @Override
    public void keyPressed(KeyEvent e) {
        scene = PlatformController.getInstance().getScene();

        if (scene.getMario().isAlive() == true) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (scene.getxPos() == -1) {
                    scene.setxPos(0);
                    scene.setBackground1PosX(-50);
                    scene.setBackground2PosX(750);
                }
                scene.getMario().setMoving(true);
                scene.getMario().setToRight(true);
                scene.setMov(GO_LEFT); // si muove verso sinistra

            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (scene.getxPos() == 4601) {
                    scene.setxPos(4600);
                    scene.setBackground1PosX(-50);
                    scene.setBackground2PosX(750);
                }

                scene.getMario().setMoving(true);
                scene.getMario().setToRight(false);
                scene.setMov(GO_RIGHT); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                scene.getMario().setJumping(true);
                AudioController.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        scene.getMario().setMoving(false);
        scene.setMov(STOP);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}

package controller;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class AudioController {
    private Clip clip;

    public AudioController(String son) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(son));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            // TODO: log error
        }
    }

    public Clip getClip() {
        return clip;
    }

    public void play() {
        clip.start();
    }

    public void stop() {
        clip.stop();
    }

    public static void playSound(String son) {
        AudioController s = new AudioController(son);
        s.play();
    }
}

package controller;

public class SceneLoopController implements Runnable {

    private final int PAUSE = 3;

    public void run() {
        while (true) {
            PlatformController.getInstance().getScene().repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }
} 
